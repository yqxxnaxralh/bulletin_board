package bulletin_board.dao;

import static bulletin_board.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Comment;
import bulletin_board.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {
		// TODO 自動生成されたメソッド・スタブ

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", message_id");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // message_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUser_id());
            ps.setInt(2, comment.getMessage_id());
            ps.setString(3, comment.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Comment> getComment(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id, ");
            sql.append("comments.user_id,");
            sql.append("comments.message_id,");
            sql.append("comments.text, ");
            sql.append("comments.created_date, ");
            sql.append("comments.updated_date,");
            sql.append("users.name as name ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	int user_id = rs.getInt("user_id");
            	int message_id = rs.getInt("message_id");
                String text = rs.getString("text");
                Timestamp created_date = rs.getTimestamp("created_date");
                Timestamp updated_date = rs.getTimestamp("updated_date");
                String name = rs.getString("name");

                Comment comment = new Comment();
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setMessage_id(message_id);
                comment.setText(text);
                comment.setCreated_date(created_date);
                comment.setUpdate_date(updated_date);
                comment.setName(name);

                ret.add(comment);

            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public void delete(Connection connection, int id) {
		// TODO 自動生成されたメソッド・スタブ
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE ");
            sql.append("FROM comments ");
            sql.append("WHERE ");
            sql.append("comments.id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            System.out.println(ps);
            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}

