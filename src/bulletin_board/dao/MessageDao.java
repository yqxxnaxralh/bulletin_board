package bulletin_board.dao;

import static bulletin_board.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Message;
import bulletin_board.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Message> getUserMessages(Connection connection, Message message, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.user_id as user_id,");
            sql.append("users.name as name,");
            sql.append("messages.subject as subject, ");
            sql.append("messages.category as category, ");
            sql.append("messages.text as text,");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("messages.created_date BETWEEN ? ");
            sql.append("AND ? ");
            sql.append("AND messages.category LIKE ? ");
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setTimestamp(1, message.getStartDate());
            ps.setTimestamp(2, message.getEndDate());
            ps.setString(3, message.getParam());
            ResultSet rs = ps.executeQuery();
            List<Message> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Message> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<Message> ret = new ArrayList<Message>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	int user_id = rs.getInt("user_id");
            	String name = rs.getString("name");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Message message = new Message();
                message.setId(id);
                message.setUser_id(user_id);
                message.setName(name);
                message.setSubject(subject);
                message.setCategory(category);
                message.setText(text);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


	public void delete(Connection connection, int id) {
		// TODO 自動生成されたメソッド・スタブ

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE ");
            sql.append("FROM messages ");
            sql.append("WHERE ");
            sql.append("messages.id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}

