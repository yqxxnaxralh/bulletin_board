package bulletin_board.dao;

import static bulletin_board.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Management;
import bulletin_board.exception.SQLRuntimeException;

public class ManagementDao {

	public List<Management> getUser(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.password, ");
            sql.append("users.account, ");
            sql.append("users.name, ");
            sql.append("users.department_id, ");
            sql.append("users.branch_id, ");
            sql.append("users.isstoped, ");
            sql.append("users.created_date, ");
            sql.append("users.updated_date, ");
            sql.append("departments.name as department, ");
            sql.append("branches.name as branch ");
            sql.append("FROM users ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("ORDER BY department_id,branch_id,account ");

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            List<Management> ret = toUserManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Management> toUserManagementList(ResultSet rs)
            throws SQLException {

        List<Management> ret = new ArrayList<Management>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	String account = rs.getString("account");
            	String name = rs.getString("name");
                int department_id = rs.getInt("department_id");
                int branch_id = rs.getInt("branch_id");
                int isstoped = rs.getInt("isstoped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");
                String department = rs.getString("department");
                String branch = rs.getString("branch");

                Management management = new Management();
                management.setId(id);
                management.setName(name);
                management.setAccount(account);
                management.setDepartment_id(department_id);
                management.setBranch_id(branch_id);
                management.setIsstoped(isstoped);
                management.setCreatedDate(createdDate);
                management.setUpdatedDate(updatedDate);
                management.setDepartment(department);
                management.setBranch(branch);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public void updateUser(Connection connection, int isstoped, int id) {
		// TODO 自動生成されたメソッド・スタブ

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE ");
            sql.append("users ");
            sql.append("SET ");
            sql.append("isstoped = ? ");
            sql.append("WHERE ");
            sql.append("id =  ? ");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, isstoped);
            ps.setInt(2, id);
            System.out.println(ps);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
}