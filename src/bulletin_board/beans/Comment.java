package bulletin_board.beans;

import java.sql.Timestamp;
import java.util.Date;


public class Comment {
	int id;
	int user_id;
	int message_id;
	String text;
    private Date created_date;
    private Date updated_date;
    String name;


	public int getId() {
		// TODO 自動生成されたメソッド・スタブ
		return this.id;
	}

	public void setId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		this.id = id;
	}

	public int getUser_id() {
		// TODO 自動生成されたメソッド・スタブ
		return this.user_id;
	}

	public void setUser_id(int user_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.user_id = user_id;
	}

	public int getMessage_id() {
		// TODO 自動生成されたメソッド・スタブ
		return this.message_id;
	}

	public void setMessage_id(int message_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.message_id = message_id;
	}

	public Date getCreatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.created_date;
	}

	public void setCreatedDate(Date createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.created_date = createdDate;
	}
	public Date getUpdatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.updated_date;
	}

	public void setUpdatedDate(Date updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updated_date = updatedDate;
	}

	public String getText() {
		// TODO 自動生成されたメソッド・スタブ
		return this.text;
	}

	public void setText(String text) {
		// TODO 自動生成されたメソッド・スタブ
		this.text = text;

	}

	public void setCreated_date(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.created_date = createdDate;
	}

	public void setUpdate_date(Timestamp updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updated_date = updatedDate;
	}

	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}

}