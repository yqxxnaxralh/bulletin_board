package bulletin_board.beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class User implements Serializable{
    private static final long serialVersionUID = 1L;

    private int id;
    private String password;
    private String account;
    private String name;
    private int department_id;
    private int branch_id;
    private int isstoped;
    private Timestamp createdDate;
    private Timestamp updatedDate;

	private String pass_ck;

	private String department;

	private String branch;

    public int getId() {
    	return this.id;
    }
	public void setId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		this.id = id;
	}
	public String getPassword() {
    	return this.password;
    }
	public void setPassword(String password) {
		// TODO 自動生成されたメソッド・スタブ
		this.password = password;
	}
	public String getAccount() {
    	return this.account;
    }
	public void setAccount(String account) {
		// TODO 自動生成されたメソッド・スタブ
		this.account = account;
	}
	public String getName() {
    	return this.name;
    }
	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}
	public int getDepartment_id() {
    	return this.department_id;
    }
	public void setDepartment_id(int department_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.department_id = department_id;
	}
	public int getBranch_id() {
    	return this.branch_id;
    }
	public void setBranch_id(int branch_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.branch_id = branch_id;
	}
	public int getIsstoped() {
    	return this.isstoped;
    }
	public void setIsstoped(int isstoped) {
		// TODO 自動生成されたメソッド・スタブ
		this.isstoped = isstoped;
	}
	public Timestamp getCreatedDate() {
    	return this.createdDate;
    }
	public void setCreatedDate(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
    	return this.updatedDate;
    }
	public void setUpdatedDate(Timestamp updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updatedDate = updatedDate;
	}

	public String getPass_ck() {
		return this.pass_ck;
	}
	public void setPass_ck(String pass_ck) {
		// TODO 自動生成されたメソッド・スタブ
		this.pass_ck = pass_ck;
	}
	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		// TODO 自動生成されたメソッド・スタブ
		this.department = department;
	}
	public String getbranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		// TODO 自動生成されたメソッド・スタブ
		this.branch = branch;
	}
}
