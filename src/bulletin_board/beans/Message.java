package bulletin_board.beans;

import java.sql.Timestamp;
import java.util.Date;

public class Message {
	int id;
	int user_id;
	String subject;
	String text;
	String category;
    private Date createdDate;
    private Date updatedDate;
	private Timestamp start;
	private Timestamp end;
	private String param;
	private String st;
	private String ed;
	private String name;


	public int getId() {
		// TODO 自動生成されたメソッド・スタブ
		return this.id;
	}

	public void setId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		this.id = id;
	}

	public int getUser_id() {
		// TODO 自動生成されたメソッド・スタブ
		return this.user_id;
	}

	public void setUser_id(int user_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.user_id = user_id;
	}
	public String getSubject() {
		// TODO 自動生成されたメソッド・スタブ
		return this.subject;
	}
	public void setSubject(String subject) {
		// TODO 自動生成されたメソッド・スタブ
		this.subject = subject;
	}

	public void setTitle(String subject) {
		// TODO 自動生成されたメソッド・スタブ
		this.subject = subject;
	}

	public String getText() {
		// TODO 自動生成されたメソッド・スタブ
		return this.text;
	}

	public void setText(String text) {
		// TODO 自動生成されたメソッド・スタブ
		this.text = text;
	}

	public String getCategory() {
		// TODO 自動生成されたメソッド・スタブ
		return this.category;
	}

	public void setCategory(String category) {
		// TODO 自動生成されたメソッド・スタブ
		this.category = category;
	}

	public Date getCreatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updatedDate = updatedDate;
	}

	public void setStartDate(Timestamp start) {
		// TODO 自動生成されたメソッド・スタブ
		this.start = start;
	}

	public void setEndDate(Timestamp end) {
		// TODO 自動生成されたメソッド・スタブ
		this.end = end;
	}

	public void setParam(String param) {
		// TODO 自動生成されたメソッド・スタブ
		this.param = param;
	}

	public Timestamp getStartDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.start;
	}

	public Timestamp getEndDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.end;
	}

	public String getParam() {
		// TODO 自動生成されたメソッド・スタブ
		return this.param;
	}

	public String getStart() {
		return this.st;
	}
	public void setStart(String st) {
		// TODO 自動生成されたメソッド・スタブ
		this.st = st;
	}

	public String getEnd() {
		return this.ed;
	}
	public void setEnd(String ed) {
		// TODO 自動生成されたメソッド・スタブ
		this.ed = ed;
	}

	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}
}
