package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;

import bulletin_board.beans.User;
import bulletin_board.dao.UserDao;
import bulletin_board.util.CipherUtil;

public class LoginService {

	public User login(String account, String password) {
		// TODO 自動生成されたメソッド・スタブ

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection,account,encPassword);

			commit(connection);

			return user;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

}
