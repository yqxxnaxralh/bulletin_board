package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Comment;
import bulletin_board.dao.CommentDao;

public class CommentService {

	public void register(Comment comment) {
		// TODO 自動生成されたメソッド・スタブ

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<Comment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            List<Comment> ret = commentDao.getComment(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//コメント削除用メソッド
	public void delete(int id) {
		// TODO 自動生成されたメソッド・スタブ
        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.delete(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}

