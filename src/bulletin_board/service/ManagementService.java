package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Management;
import bulletin_board.dao.ManagementDao;

public class ManagementService {

	public List<Management> getManagement() {

		Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao managementdao = new ManagementDao();
            List<Management> ret = managementdao.getUser(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void Update(int isstoped, int id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao managementdao = new ManagementDao();
            managementdao.updateUser(connection,isstoped,id);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
