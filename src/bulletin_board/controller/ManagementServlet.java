package bulletin_board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletin_board.beans.Management;
import bulletin_board.service.ManagementService;

/**
 * Servlet implementation class ManagementServlet
 */
@WebServlet("/management")
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManagementServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		List<Management> management = new ManagementService().getManagement();

		request.setAttribute("userlist", management);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		int id = Integer.parseInt(request.getParameter("id"));
		int isstoped;

		int number = Integer.parseInt(request.getParameter("management"));
		if (number == 1) {
			isstoped = 1;
		} else {
			isstoped = 0;
		}

		new ManagementService().Update(isstoped, id);

		List<Management> management = new ManagementService().getManagement();
		request.setAttribute("userlist", management);
		request.getRequestDispatcher("management.jsp").forward(request, response);

	}
}
